## General
Little notecard program for practicing Kanji.

## Credits
Kanji data scraped off https://kanjicards.org/ with kanjicard_parser (add link to git repo)
This site further credits http://www.edrdg.org/ and https://sites.google.com/site/nihilistorguk/

## Todo
 - add more lists
 - move from pick list to direct display
 - make interface prettier
 - make kanji in examples clickable to navigate there