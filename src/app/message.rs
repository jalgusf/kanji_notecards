#[derive(Debug, Clone)]
pub enum Message {
    ChooseList(String),
    BackToList,
    Randomize,
    OpenLink(String),
    ShowKanji(char)
}