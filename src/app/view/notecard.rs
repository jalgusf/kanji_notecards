use crate::App;
use crate::app::Message;

use iced::{
	Button, Column, Row, Text, Font
};

pub fn create_view(application: &mut App) -> iced::Element<'_, Message>
{
	let mut first_column = Column::new();
	let mut current_row = Row::new();
	for (i, (k, btn_state)) in application.current_kanji.iter().zip(application.kanji_button_states.iter_mut()).enumerate() {
		let k_button = Button::new(btn_state, Text::new(format!("{}",k)).size(60))
			.on_press(Message::ShowKanji(*k));
		current_row = current_row.push(k_button);
		if i % 4 == 3 {
			first_column = first_column.push(current_row);
			current_row = Row::new();
		}
	}
	let rand_button = Button::new(&mut application.randomize_button_state, Text::new("Randomize"))
				.on_press(Message::Randomize);
	let back_button = Button::new(&mut application.list_button_state, Text::new("Back"))
				.on_press(Message::BackToList);
	first_column = first_column.push(Row::new().push(rand_button))
		.push(Row::new().push(back_button));
	let mut view = Row::new().push(first_column);
	
	if application.chosen_kanji.is_some() {
		let kanji_char = application.chosen_kanji.unwrap();
		let kanji_codepoint = format!("{:x}",kanji_char as u32);
		
		let wadoku_button = Button::new(&mut application.wadoku_button_state, Text::new(format!("{} in wadoku", kanji_char)))
					.on_press(Message::OpenLink(format!("https://www.wadoku.de/search/{}",kanji_char)));
		let mut col = Column::new();
		let mut first_row = Row::new().push(Column::new().push(Row::new()
								.push(Text::new(format!("{}",kanji_char)).size(200).font(
									Font::External{
										name: "stroke order font",
										bytes: include_bytes!("../../../assets/fonts/kanji_stroke_order_font/KanjiStrokeOrders_v4.004.ttf")
									}))
								));
		if application.kanji_complete.contains_key(&kanji_codepoint){
			let content = application.kanji_complete.get(&kanji_codepoint).unwrap();
			let reading: String;
			if content.on.is_none() {
				reading = format!("{}",content.kun.as_ref().unwrap());
			}else if content.kun.is_none() {
				reading = format!("{}",content.on.as_ref().unwrap());
			}else{
				reading = format!("{} {}",content.on.as_ref().unwrap(),content.kun.as_ref().unwrap());
			}
			
			first_row = first_row.push(Column::new()
								.push(Row::new().push(Text::new(format!("{}",reading))))
								.push(Row::new().push(Text::new(format!("{}",&content.meaning)))));
			
			col = col.push(first_row)
					.push(Row::new().push(Text::new(String::from("----------------"))));
			
			application.kanji_example_button_states.clear();
			for (i, ex) in content.examples.iter().enumerate() {
				application.kanji_example_button_states.push(vec![]);
				for _t in ex.kanji.chars() {
					application.kanji_example_button_states[i].push(iced::button::State::new());
				}
			}
			
			for (ex, btn_state_vec) in content.examples.iter().zip(application.kanji_example_button_states.iter_mut()) {
				let mut r = Row::new();
				for (t, btn_state) in ex.kanji.chars().zip(btn_state_vec.iter_mut()) {
					if kanji::is_kanji(&t) {
						r = r.push(Button::new(btn_state, Text::new(format!("{}",t)).size(60))
							.on_press(Message::ShowKanji(t)));
					}else {
						r = r.push(Text::new(format!("{}",t)).size(60));
					}
				}
				col = col.push(Row::new().push(Column::new().push(r))
							.push(Column::new()
								.push(Row::new().push(Text::new(format!("{}",ex.reading))))
								.push(Row::new().push(Text::new(format!("{}",ex.meaning))))));
			}
		}else{
			col = col.push(first_row)
					.push(Row::new().push(Text::new(String::from("----------------"))));
			let stroke_order_button = Button::new(&mut application.stroke_order_button_state, Text::new(format!("{} Strichfolge", kanji_char)))
					.on_press(Message::OpenLink(format!("https://www.bibiko.de/kanji/sod/{}.html",kanji_codepoint)));
			col = col.push(Row::new().push(Text::new(format!("{}",kanji_char)).size(60)))
				.push(Row::new().push(stroke_order_button).push(Text::new(format!("https://www.bibiko.de/kanji/sod/{}.html",kanji_codepoint))));
		}
		
		col = col.push(Row::new().push(wadoku_button).push(Text::new(format!("https://www.wadoku.de/search/{}",kanji_char))));
			
		// https://lingweb.eva.mpg.de/kanji/
		// https://kanjicards.org/img/stroke_order/<kanjicards_id>.png
		// https://kanjicards.org/download-kanji-cards-with-stroke-order.html
		view = view.push(col);
	}
	view.into()
}