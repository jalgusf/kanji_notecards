use crate::App;
use crate::app::Message;

use iced::{Column, Row, PickList, Text};

pub fn create_view(application: &mut App) -> iced::Element<'_, Message>
{
	Column::new()
		.push(Row::new()
			.push(Text::new("Choose a list: "))
			.push(PickList::new(&mut application.list_state, application.lists.clone(), None, Message::ChooseList)))
		.into()
}