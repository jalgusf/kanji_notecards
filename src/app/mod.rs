mod view;
mod message;
mod kanji_data;

use message::Message;

use rand::seq::SliceRandom;
use std::collections::HashMap;

use webbrowser;

pub struct App {
	chosen_list: Option<String>,
	chosen_kanji: Option<char>,
	kanji_complete: HashMap<String, kanji_data::Kanji>,
	current_kanji_complete: std::vec::Vec<char>,
	current_kanji: std::vec::Vec<char>,
	list_state: iced::pick_list::State<String>,
	lists: std::vec::Vec<String>,
	list_button_state: iced::button::State,
	randomize_button_state: iced::button::State,
	wadoku_button_state: iced::button::State,
	stroke_order_button_state: iced::button::State,
	kanji_button_states: std::vec::Vec<iced::button::State>,
	kanji_example_button_states: std::vec::Vec<std::vec::Vec<iced::button::State>>,
	max_shown_kanji: usize
}

impl App {
	fn randomize(&mut self){
		self.current_kanji = self.current_kanji_complete
			.choose_multiple(&mut rand::thread_rng(), self.max_shown_kanji)
			.cloned()
			.collect();
	}
	
	fn get_lists(&mut self) -> (){
		let mut exe_dir = std::env::current_exe().unwrap();
		exe_dir.pop();
		let path_iterator = std::fs::read_dir(exe_dir.join("data/list")).map_err(|err| {
			return format!("can not read list data path - {:?}",err);
		}).unwrap();
		self.lists = path_iterator.map(|x| x.unwrap().file_name().into_string().unwrap()).collect();
	}
	
	fn get_list_content(&mut self) -> (){
		let mut exe_dir = std::env::current_exe().unwrap();
		exe_dir.pop();
		self.current_kanji_complete = std::fs::read_to_string(exe_dir.join("data/list").join(self.chosen_list.as_ref().expect("No list chosen").as_str())).unwrap().chars().collect();
	}
	
	fn load_kanji_complete(&mut self) -> (){
		let mut exe_dir = std::env::current_exe().unwrap();
		exe_dir.pop();
		let json = std::fs::read_to_string(exe_dir.join("data/kanji/complete.json")).unwrap();
		self.kanji_complete = serde_json::from_str(&json).unwrap();
	}
}

impl iced::Application for App {
    type Executor = iced::executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: ()) -> (Self, iced::Command<Message>) {
		let mut app = App {
			chosen_list: Some(String::from("all.txt")),
			chosen_kanji: None,
			kanji_complete: HashMap::new(),
			current_kanji_complete: vec![],
			current_kanji: vec![],
			lists: vec![],
			list_state: Default::default(),
			list_button_state: Default::default(),
			randomize_button_state: Default::default(),
			wadoku_button_state: Default::default(),
			stroke_order_button_state: Default::default(),
			kanji_button_states: vec![],
			kanji_example_button_states: vec![],
			max_shown_kanji: 20
		};
		app.kanji_button_states = vec![iced::button::State::new(); app.max_shown_kanji];
		app.get_lists();
		app.load_kanji_complete();
		
		if app.chosen_list.is_some() {
			app.get_list_content();
			app.randomize();
		}
		
		(app,iced::Command::none())
    }

    fn title(&self) -> String {
        String::from("Kanji notecards")
    }

    fn update(&mut self, message: Message, _clipboard: &mut iced::Clipboard) -> iced::Command<Message> {
		match message {
			Message::ChooseList(val) => {
				self.chosen_list = Some(val);
				self.get_list_content();
				self.chosen_kanji = None;
				self.randomize();
			}
			Message::BackToList => {
				self.chosen_list = None;
				self.chosen_kanji = None;
			}
			Message::Randomize => {
				self.randomize();
			}
			Message::ShowKanji(val) => {
				self.chosen_kanji = Some(val);
			}
			Message::OpenLink(val) => {
				if webbrowser::open(&val).is_err() {
					eprintln!("Could not open link {}",&val);
				}
			}
		}
		iced::Command::none()
    }

    fn view(&mut self) -> iced::Element<'_, Message> {
		if self.chosen_list.is_none(){
			return view::list::create_view(self);
		}
		view::notecard::create_view(self)
    }
}