use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct KanjiExample {
	pub kanji: String,
	pub reading: String,
	pub meaning: String
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Kanji {
	pub kanji: char,
	pub kanjicards_id: u16,
	pub on: Option<String>,
	pub kun: Option<String>,
	pub meaning: String,
	pub codepoint: String,
	pub examples: std::vec::Vec<KanjiExample>
}