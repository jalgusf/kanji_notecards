#![forbid(unsafe_code)]
#![warn(rust_2018_idioms,trivial_casts,trivial_numeric_casts,missing_debug_implementations)]

mod app;

use app::App;
use iced::{Application, Settings, Result};

fn main() -> Result{
	let mut settings = Settings::default();
	settings.default_font = Some(include_bytes!("../assets/fonts/hannari/HannariMincho-Regular.otf"));
	settings.default_text_size = 30;
	settings.window.size = (1600,900);
    App::run(settings)
}